/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_types.h"
#include "xil_printf.h"
#include "xgpiops.h"

void writePins(const XGpioPs *InstancePtr, u32 value)
{
    // PMOD 0-4 <-> MIO 13,10,11,12
    XGpioPs_WritePin(InstancePtr, 13, (value & 1<<0)>>0);
    XGpioPs_WritePin(InstancePtr, 10, (value & 1<<1)>>1);
    XGpioPs_WritePin(InstancePtr, 11, (value & 1<<2)>>2);
    XGpioPs_WritePin(InstancePtr, 12, (value & 1<<3)>>3);
}

int main()
{
    init_platform();

    print("Hello World\n\r");

    XGpioPs_Config xgp_c = {.DeviceId = XPAR_XGPIOPS_0_DEVICE_ID, .BaseAddr = XPAR_XGPIOPS_0_BASEADDR};
    XGpioPs xgp;
    XGpioPs_CfgInitialize(&xgp, &xgp_c, XPAR_XGPIOPS_0_BASEADDR);
    
    XGpioPs_SetDirectionPin(&xgp, 13, 1);
    XGpioPs_SetOutputEnablePin(&xgp, 13, 1);
    XGpioPs_SetDirectionPin(&xgp, 10, 1);
    XGpioPs_SetOutputEnablePin(&xgp, 10, 1);
    XGpioPs_SetDirectionPin(&xgp, 11, 1);
    XGpioPs_SetOutputEnablePin(&xgp, 11, 1);
    XGpioPs_SetDirectionPin(&xgp, 12, 1);
    XGpioPs_SetOutputEnablePin(&xgp, 12, 1);

    u32 ctr = 0;
    while(1)
    {
        writePins(&xgp, ctr);
        ctr++;

		for(int i = 0; i < 100000000; i++)
		{
			asm("nop");
		}
    }

    cleanup_platform();
    return 0;
}
